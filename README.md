Vagrant tworzy maszynę wirtualną z pustą bazą danych `test` oraz użytkownikami:

* u: `test`, p:
* u: `testy`, p: `testy`


Wymagania:

* [Vagrant](https://www.vagrantup.com/)
* [VirtualBox](https://www.virtualbox.org/)

Użycie:

* wchodzimy do folderu z projektem -> cmd
* wykonujemy komendę: 
```
#!shell

vagrant up
```

Przydatne komendy:

* zatrzymanie maszyny wirtualnej: vagrant halt
* zniszczenie maszyny wirtualnej: vagrant destroy
* ponowny provisioning maszyny wirtualnej: vagrant provision

Podczas wykonywania komend VirtualBox powinien być uruchomiony.