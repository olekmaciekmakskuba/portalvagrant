$enhancers = ["php5", "php5-curl", "php5-mysql", "php5-mcrypt", "phpmyadmin"]

$override_options = {
  'mysqld' => {
    'bind_address'             => '0.0.0.0',
  }
}

service{ "apache2":
  ensure => "running",
  enable => true,
  require => Package["apache2"]
}

Package{
  ensure => "installed"
}

exec { "apt-get update":
    command => "/usr/bin/apt-get update",
}->
package{ "apache2":
}->
package{ $enhancers:
}->
mysql_database { 'test':
  ensure  => 'present',
  charset => 'utf8',
  collate => 'utf8_general_ci',
}->
mysql_user { "test@%":
  ensure                   => 'present',
  max_connections_per_hour => '5000',
  max_queries_per_hour     => '5000',
  max_updates_per_hour     => '5000',
  max_user_connections     => '100',
}->
mysql_grant { "test@%/*.*":
  ensure     => 'present',
  privileges => ['ALL'],
  table      => '*.*',
  user       => "test@%",
}->
mysql_user { "testy@%":
  ensure                   => 'present',
  max_connections_per_hour => '5000',
  max_queries_per_hour     => '5000',
  max_updates_per_hour     => '5000',
  max_user_connections     => '100',
  password_hash				=> mysql_password('testy')
}->
mysql_grant { "testy@%/*.*":
  ensure     => 'present',
  privileges => ['ALL'],
  table      => '*.*',
  user       => "testy@%",
}

class { 'mysql::server':
  override_options			=> $override_options,
  restart 					=> true
}->
file{'/etc/apache2/conf.d/phpmyadmin.conf':
   ensure => 'link',
   target => '/etc/phpmyadmin/apache.conf',
   notify  => Service["apache2"]
}
